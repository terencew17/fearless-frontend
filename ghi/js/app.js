function createCard(name, description, pictureUrl, date, location) {
    return `
    <div class="col sm-2">
      <div class="card shadow p-3 mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer">${date}</div>
        </div>
      </div>
    </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    // const response = await fetch(url);
    // console.log(response);

    // const data = await response.json();
    // console.log(data);

    try {
        const response = await fetch(url);

        if (!response.ok) {
          var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
          var alertTrigger = document.getElementById('liveAlertBtn')

          function alert(message, type) {
            var wrapper = document.createElement('div')
            wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

            alertPlaceholder.append(wrapper)
          }

          if (alertTrigger) {
            alertTrigger.addEventListener('click', function () {
              alert('Error! Please try again later!', 'success')
            })
          }
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
              const detailUrl = `http://localhost:8000${conference.href}`;
              const detailResponse = await fetch(detailUrl);
              if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;

                const location = details.conference.location.name;

                const startDate = new Date(details.conference.starts);
                const start = `${startDate.getMonth()}/${startDate.getDate()}/${startDate.getFullYear()}`;
                const endDate = new Date(details.conference.ends);
                const end = `${endDate.getMonth()}/${endDate.getDate()}/${endDate.getFullYear()}`;
                const date = `${start}-${end}`;

                const html = createCard(title, description, pictureUrl, date, location);
                console.log(html);
                const column = document.querySelector('.row');
                column.innerHTML += html;
              }
            }
          }
    } catch (e) {
        console.error('error', e)
    }
});
